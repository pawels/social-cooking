
/**
 * Module dependencies.
 */

var express = require('express');
var fs = require('fs');
var http = require('http');
var path = require('path');
var mongodb = require('mongodb');
var env = process.env.NODE_ENV || 'development';
var config = require('./config/config')[env];


//db  
var mongoose = require('mongoose');
mongoose.connect(config.db);
// Bootstrap models
var models_path = __dirname + '/app/models'
fs.readdirSync(models_path).forEach(function (file) {
  require(models_path+'/'+file)
})


//app
var app = express();
// express settings
require('./config/express')(app, config);
require('./routes')(app);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
