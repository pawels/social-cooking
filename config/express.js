var express = require('express');

module.exports = function (app, config) {
	
	
	
	// all environments
	app.set('port', process.env.PORT || 3000);
	app.set('title', config.app.name);
	
	app.use(express.logger('dev'));
	app.use(express.bodyParser());
	//app.use(express.json());
//app.use(express.urlencoded());
	app.use(express.methodOverride());
	app.use(express.cookieParser('SDFG%#$GFgasdfio34h08rqjwpdj982423asSA#@'));
	app.use(express.session());
	app.use(app.router);

	// development only
	if ('development' == app.get('env')) {
	  app.use(express.errorHandler());
	}
};