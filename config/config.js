
module.exports = {
  development: {
      db: 'mongodb://localhost/scook',
    app: {
      name: 'social-cooking'
    } 
  },
  test: {
    db: process.env.MONGOHQ_URL,
    app: {
      name: 'social-cooking'
    } 
  },
  production: {
  	db: process.env.MONGOHQ_URL,
    app: {
      name: 'social-cooking'
    } 
  }
}
