/**
 * Created by pawel on 16.11.2013.
 */

var mongoose = require('mongoose');
var Recipe = mongoose.model('Recipe');
var helpers = require('../../helpers');
var _ = require('underscore');
var fs = require('fs'),
    path = require('path');

var fileUploadPath = path.resolve('uploads');
var fileupload = require('fileupload').createFileUpload(fileUploadPath).middleware;

//routes
module.exports = function(app) {
    app.get('/recipes', findAll);
    app.get('/recipes/search', search);
    app.post('/recipes', fileupload, create);

    app.get('/recipes/:id', findRecipe);
    app.get('/recipes/:id/picture', getRecipeImage);
    //app.get('/recipes/picture/:file', getRecipeFile);
    app.post('/recipes/:id', update);
    app.delete('/recipes/:id', remove);
}

/**
 * List all of all recipes or search
 * Pagiination by GET param "page", starts from 0
 * Search by GET param "query"
 * @param req
 * @param res
 */
var findAll = function(req, res) {
    var page = req.param('page') > 0 ? req.param('page') : 0;
    var query = (req.param('query') && req.param('query') != '')  ? req.param('query') : null;
    var perPage = 100;
    var options = {
        perPage: perPage,
        page: page
    }
    if(query) {
        var nameReg = new RegExp(query, 'i');
        options.criteria = {
            $or: [ { name: nameReg }, { tags: { $in: [ nameReg ] } } ]
        };
    }
    Recipe.list(options, function(err, products) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }
        else {
            res.send(products);
        }
    });
};


/**
 * List all of all recipes
 * Pagiination by GET param "page", starts from 0
 * @param req
 * @param res
 */
var search = function(req, res) {
    var page = req.param('page') > 0 ? req.param('page') : 0;
    var products = (req.param('products') && req.param('products') != '')  ? req.param('products') : null;
    var perPage = 100;
    var options = {
        perPage: perPage,
        page: page,
        products: null
    }
    if(products) {
        options.criteria = {products: { $in: products }};
        console.log(products);
    }
    Recipe.search(options, function(err, products) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }
        else {
            res.send(products);
        }
    });

};


/**
 * Create recipe
 * @param req
 * @param res
 */
var create = function(req, res) {
    req.body.products = req.body.products[0];
    var recipe = new Recipe(req.body);
    recipe.save(function (err, recipe) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }else {
            req.params.id = recipe._id;
            findRecipe(req, res);
        }
    });
};


/**
 * Get recipe by ID
 * @param req
 * @param res
 */
var findRecipe = function (req, res) {
    Recipe.load(req.params.id, function (err, recipe) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }else {
            res.send(recipe);
        }
    });
}

/**
 * Get recipe by ID
 * @param req
 * @param res
 */
var getRecipeImage = function (req, res) {
    Recipe.load(req.params.id, function (err, recipe) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }else {
            if(recipe.image) {
                res.sendfile(fileUploadPath+'/'+recipe.image[0].path+recipe.image[0].basename);
            }
            else {
                res.send(404);
            }

        }
    });
}


/**
 * Update recipe info
 * @param req
 * @param res
 */
var update = function (req, res) {
    Recipe.load(req.params.id, function (err, recipe) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }else {
            recipe = _.extend(product, req.body);
            recipe.save(function (err, product) {
                if (err) {
                    console.log(err.stack);
                    return res.send('500', err.stack);
                }else {
                    res.send(recipe);
                }
            });
        }
    });
}

/**
 * remove recipe
 * @param req
 * @param res
 */
var remove = function (req, res) {
    Recipe.load(req.params.id, function (err, recipe) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }else {
            if(recipe) {
                recipe.remove(function(err) {
                    var responseBody = {
                        "status": "ok",
                        "msg": "Recipe #"+req.params.id+" deleted"
                    };
                    return res.send('200', responseBody);
                });
            }
            else {
                var responseBody = {
                    "status": "error",
                    "msg": "Recipe #"+req.params.id+" not found"
                };
                return res.send('404', responseBody);
            }
        }
    });
}