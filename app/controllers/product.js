/**
 * Controller Prodcut - manage products
 * @type {exports}
 */

var mongoose = require('mongoose');
var Product = mongoose.model('Product');
var _ = require('underscore');
var helpers = require('../../helpers');

//routes
module.exports = function(app) {
    app.get('/products', helpers.auth, findAll);
	app.post('/products', helpers.auth, create);
	app.get('/products/:id', helpers.auth, findProduct);
	app.post('/products/:id', helpers.auth, update);
    app.delete('/products/:id', helpers.auth, remove);
}

/**
 * List all of all products
 * Pagiination by GET param "page", starts from 0
 * @param req
 * @param res
 */
var findAll = function(req, res) {
	var page = req.param('page') > 0 ? req.param('page') : 0;
    var query = (req.param('query') && req.param('query') != '')  ? req.param('query') : null;
	var perPage = 100;
	var options = {
		perPage: perPage,
		page: page
	}
    if(query) {
        var nameReg = new RegExp(query, 'i');
        options.criteria = {
            $or: [ { name: nameReg }, { tags: { $in: [ nameReg ] } } ]
        };
    }

	Product.list(options, function(err, products) {
		if (err) {
			console.log(err.stack);
			return res.send('500', err.stack);
		}   
		else {
            res.send(products);
		} 
	});
	
};


/**
 * Create product
 * @param req
 * @param res
 */
var create = function(req, res) {
	var product = new Product(req.body);
	product.save(function (err, product) {
		if (err) {
			console.log(err.stack);
			return res.send('500', err.stack);
		}else {
            req.params.id = product._id;
            findProduct(req, res);
			 //res.send(product.populate('user').exec());
		}	
	});
};


/**
 * Get product by ID
 * @param req
 * @param res
 */
var findProduct = function (req, res) {
	Product.load(req.params.id, function (err, product) {
		if (err) {
			console.log(err.stack);
			return res.send('500', err.stack);
		}else {
			 res.send(product); 	
		}	
	});
}


/**
 * Update product info
 * @param req
 * @param res
 */
var update = function (req, res) {
        Product.load(req.params.id, function (err, product) {
            if (err) {
                console.log(err.stack);
                return res.send('500', err.stack);
            }else {
                product = _.extend(product, req.body);
                product.save(function (err, product) {
                    if (err) {
                        console.log(err.stack);
                        return res.send('500', err.stack);
                    }else {
                        findProduct(req, res);
                    }
                });
            }
        });
    }

/**
 * remove product
 * @param req
 * @param res
 */
var remove = function (req, res) {
    Product.load(req.params.id, function (err, product) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }else {
            if(product) {
                product.remove(function(err) {
                    var responseBody = {
                        "status": "ok",
                        "msg": "Product #"+req.params.id+" deleted"
                    };
                    return res.send('200', responseBody);
                 });
            }
            else {
                var responseBody = {
                    "status": "error",
                    "msg": "Product #"+req.params.id+" not found"
                };
                return res.send('404', responseBody);
            }
        }
    });
}