/**
 * Controller User - manage user
 * @type {exports}
 */

var mongoose = require('mongoose');
var User = mongoose.model('User');
var Product = mongoose.model('Product');
var Recipe = mongoose.model('Recipe');
var helpers = require('../../helpers');


module.exports = function (app) {
    app.get('/users', findAll);
    app.post('/users', create);
    app.get('/users/:id', findUser);
    app.get('/users/email/:email', findUserByEmail);
    app.get('/users/:id/products', findUserProducts);
    app.get('/users/:id/recipes', findUserRecipes);



    app.get('/users/:id/update', updateTest);
}


/**
 * List all of users
 * Pagination by GET param "page", starts from 0
 * @param req
 * @param res
 */
var findAll = function (req, res) {
    var page = req.param('page') > 0 ? req.param('page') : 0
    var perPage = 3;
    var options = {
        perPage: perPage,
        page: page
    }
    User.list(options, function (err, users) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }
        else {
            res.send(users);
        }
    });

};

/**
 * Create new user
 * @param req
 * @param res
 */
var create = function (req, res) {
    var user = new User(req.body)
    user.provider = 'local'
    user.save(function (err) {
        if (err) {
            console.error(err.stack);
            return res.send('500', err.stack);
        }
        req.params.id = user._id;
        findUser(req, res);
    })
};


/**
 * Get user data by ID
 * @param req
 * @param res
 */
var findUser = function(req, res) {
    User.load(req.params.id, function (err, user) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }else {
            res.send(user);
        }
    });
}


/**
 * Get user data by emailaddress
 * @param req
 * @param res
 */
var findUserByEmail = function(req, res) {
    User.loadByEmail(req.params.email, function (err, user) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }else {
            if(user) {
                res.send(user);
            }
            else {
                var responseBody = {
                    "status": "error",
                    "msg": "User not found"
                };
                res.send(responseBody);
            }

        }
    });
}


/**
 * Get recipes added by user
 * @param req
 * @param res
 */
var findUserRecipes = function (req, res) {
    var page = req.param('page') > 0 ? req.param('page') : 0
    var perPage = 10;
    var options = {
        perPage: perPage,
        page: page,
        criteria: {user: req.params.id }
    }
    Recipe.list(options, function(err, recipes) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }
        else {
            res.send(recipes);
        }
    });
};


/**
 * Get products added by user
 * @param req
 * @param res
 */
var findUserProducts = function (req, res) {
    var page = req.param('page') > 0 ? req.param('page') : 0
    var perPage = 10;
    var options = {
        perPage: perPage,
        page: page,
        criteria: {user: req.params.id }
    }
    Product.list(options, function(err, products) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }
        else {
            res.send(products);
        }
    });
}


/**
 * Update for test
 * @param req
 * @param res
 */
var updateTest = function (req, res) {
    User.load(req.params.id, function (err, user) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }else {
            user.facebook = {"id" : "100000205138622"};
            user.save();
            res.send(user);
        }
    });
}
