/**
 * Controller Applications - manage applications
 * @type {exports}
 */

var mongoose = require('mongoose');
var Application = mongoose.model('Application');
var _ = require('underscore');
var helpers = require('../../helpers');

var express = require('express');


//routes
module.exports = function(app) {
    app.get('/applications', findAll);
    app.get('/applications/geo', geo);
	app.post('/applications', create);
	app.get('/applications/:id', findApplication);
	app.post('/applications/:id', update);


    //app.post('/applications/:id/update', updateTest);
}

/**
 * List all of all applications
 * Pagiination by GET param "page", starts from 0
 * @param req
 * @param res
 */
var findAll = function(req, res) {
	var page = req.param('page') > 0 ? req.param('page') : 0;
    var query = (req.param('query') && req.param('query') != '')  ? req.param('query') : null;
    var latitude = (req.param('latitude') && req.param('latitude') != '')  ? req.param('latitude') : null;
    var longitude = (req.param('longitude') && req.param('longitude') != '')  ? req.param('longitude') : null;


	var perPage = 100;
	var options = {
		perPage: perPage,
		page: page
	}
    if(query) {
        var nameReg = new RegExp(query, 'i');
        options.criteria = {
            $or: [ { tags: { $in: [ nameReg ] } } ]
        };
    }
    console.log('latitude: '+latitude);
    console.log('longitude: '+longitude);
    if(latitude && longitude) {
        console.log('latlong');
        options.criteria = { loc : { $near :
        { $geometry :
        { type : "Point" ,
            coordinates: [ longitude , latitude ] } },
            $maxDistance : 5000
        } }
    }


	Application.list(options, function(err, applications) {
		if (err) {
			console.log(err.stack);
			return res.send('500', err.stack);
		}   
		else {
            console.log(__dirname);
			res.send(applications);
		} 
	});
	
};


/**
 * List all of all applications
 * Pagiination by GET param "page", starts from 0
 * @param req
 * @param res
 */
var geo = function(req, res) {

    var latitude = (req.param('latitude') && req.param('latitude') != '')  ? req.param('latitude') : null;
    var longitude = (req.param('longitude') && req.param('longitude') != '')  ? req.param('longitude') : null;


    var perPage = 100;

    var options = {
        "longitude": longitude,
        "latitude": latitude
    };

    Application.geo(options, function(err, applications) {
        if (err) {
            console.log(err.stack);
            return res.send('500', err.stack);
        }
        else {
            console.log(applications);
            res.send(applications);
        }
    });

};


/**
 * Create application
 * @param req
 * @param res
 */
var create = function(req, res) {
    var app = req.body;
    _.each(app.loc.coordinates, function(i, j){
        console.log(i);
        console.log(j);
        app.loc.coordinates[j] = parseFloat(i);
    });

    console.log(app);
    console.log(req.body);

	var application = new Application(req.body);
    application.save(function (err, product) {
		if (err) {
			console.log(err.stack);
			return res.send('500', err.stack);
		}else {
            req.params.id = application._id;
            return findApplication(req, res);
		}
	});
};


/**
 * Get application by ID
 * @param req
 * @param res
 */
var findApplication = function (req, res) {
	Application.load(req.params.id, function (err, application) {
		if (err) {
			console.log(err.stack);
			return res.send('500', err.stack);
		}else {
            return res.send(application);
		}	
	});
}


/**
 * Update product info
 * @param req
 * @param res
 */
var update = function (req, res) {
	Application.load(req.params.id, function (err, application) {
		if (err) {
			 console.log(err.stack);
			return res.send('500', err.stack);
		}else {
            var app = req.body;
            _.each(app.loc.coordinates, function(i, j){
                app.loc.coordinates[j] = parseFloat(i);
            });
            application = _.extend(application, app);
            application.save(function (err, product) {
				if (err) {
					console.log(err.stack);
					return res.send('500', err.stack);
				}else {
					 res.send(application);
				}
			});
		}
	});
}