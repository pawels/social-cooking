/**
 * Created by pawel on 16.11.2013.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var helpers = require('../../helpers');
var validate = require('mongoose-validator').validate;


/**
 * Application Schema
 */
var applicationSchema;
applicationSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true},
    //list of products
    products: [
        { type: Schema.Types.ObjectId, ref: 'Product', required: true }
    ],
    //coordinates
    loc: {
        type: { type: String, required: true },
        coordinates: {type: Array, required: true}
    },
    tags: {type: []},
    created: { type: Date, default: Date.now },
    updated: { type: Date }
});
//add 2dspare index
applicationSchema.index({ loc : '2dsphere' });
/*
 * Add slug

applicationSchema.pre('validate', function (next) {
    this.slug = helpers.slug(this.name);
    next();
});
*/


/*
 * Updated
 */
applicationSchema.pre('save', function (next) {
    this.updated = new Date().toISOString();
    next();
});


/**
 * Statics
 */

applicationSchema.statics = {

    /**
     * Find recpie by id
     *
     * @param {ObjectId} id
     * @param {Function} cb
     * @api private
     */

    load: function (id, cb) {
        this.findOne({ _id: id }).populate('user').populate('products').exec(cb);
    },

    /**
     *
     * @param slug - slug of recpie
     * @param cb
     */
    bySlug: function (slug, cb) {
        this.findOne({'slug': slug}).populate('user').exec(cb);
    },

    /**
     * List of Applications
     *
     * @param {Object} options
     * @param {Function} cb
     * @api private
     */

    geo: function (options, cb) {
        var criteria = options.criteria || {};

        this.find({
            loc : { $near :
            { $geometry :
            { type : "Point" ,
                coordinates: [ options.longitude , options.latitude ] } },
                $maxDistance : 1000 * 16000
            }
        }).populate('user').exec(cb);

//        this.find(criteria)
//            .populate('user')
//            .sort({'created': -1}) // sort by date
//            .limit(options.perPage)
//            .skip(options.perPage * options.page)
//            .exec(cb)
    },

    /**
     * List of Applications
     *
     * @param {Object} options
     * @param {Function} cb
     * @api private
     */

    list: function (options, cb) {
        var criteria = options.criteria || {}
        this.find(criteria)
            .populate('user')
            .sort({'created': -1}) // sort by date
            .limit(options.perPage)
            .skip(options.perPage * options.page)
            .exec(cb)
    }
}


applicationSchema.post('init', function (doc) {
    console.log('%s has been initialized from the db', doc._id);
})
applicationSchema.post('validate', function (doc) {
    console.log('%s has been validated (but not saved yet)', doc._id);
})
applicationSchema.post('save', function (doc) {
    console.log('%s has been saved', doc._id);
})
applicationSchema.post('remove', function (doc) {
    console.log('%s has been removed', doc._id);
})


mongoose.model('Application', applicationSchema);