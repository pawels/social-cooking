var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var helpers = require('../../helpers');
var validate = require('mongoose-validator').validate;


/**
 * Validations
 */
var nameValidator = [validate({message: '"name" should be between 1 and 255 characters'},'len', 1, 255)];

/**
 * Product Schema
 */
var productSchema = new Schema({
	name: { type: String, required: true, trim: true,  index: { unique: true }, validate:  nameValidator},	
	slug: { type: String, trim: true,  required: true, index: { unique: true }},
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true},
	unit: {
		name: { type: String, required: false, trim: true },
		value: { type: String, required: false, trim: true }
	},
	tags: {type: []},
	created: { type: Date, default: Date.now },
	updated: { type: Date }
});
/*
* Add slug
*/
productSchema.pre('validate', function (next) {
    this.slug = helpers.slug(this.name);
    next(); 
});


/*
* Updated
*/
productSchema.pre('save', function (next) {
    this.updated = new Date().toISOString();
    next(); 
});




/**
 * Statics
 */

productSchema.statics = {

  /**
   * Find product by id
   *
   * @param {ObjectId} id
   * @param {Function} cb
   * @api private
   */

  load: function (id, cb) {
    this.findOne({ _id : id }).populate('user')
      .exec(cb)
  },

  /**
   * List products
   *
   * @param {Object} options
   * @param {Function} cb
   * @api private
   */

  list: function (options, cb) {
    var criteria = options.criteria || {}
      console.log(criteria);
    this.find(criteria)
      .populate('user', 'name _id email username')
      .sort({'created': -1}) // sort by date
      .limit(options.perPage)
      .skip(options.perPage * options.page)
      .exec(cb)
  }

}




productSchema.post('init', function (doc) {
  console.log('%s has been initialized from the db', doc._id);
})
productSchema.post('validate', function (doc) {
  console.log('%s has been validated (but not saved yet)', doc._id);
})
productSchema.post('save', function (doc) {
  console.log('%s has been saved', doc._id);
})
productSchema.post('remove', function (doc) {
  console.log('%s has been removed', doc._id);
})


mongoose.model('Product', productSchema);