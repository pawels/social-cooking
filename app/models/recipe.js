var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var helpers = require('../../helpers');
var validate = require('mongoose-validator').validate;


/**
 * Validations
 */
var nameValidator = [validate({message: '"name" should be between 1 and 255 characters'}, 'len', 1, 255)];

/**
 * Recipe Schema
 */
var recipeSchema;
recipeSchema = new Schema({
    name: { type: String, required: true, trim: true, index: { unique: true }, validate: nameValidator},
    slug: { type: String, trim: true, required: true, index: { unique: true }},
    products: [
        { type: Schema.Types.ObjectId, ref: 'Product', required: true}
    ],
    image: { type: Object, required: true},
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true},
    tags: {type: []},
    created: { type: Date, default: Date.now },
    updated: { type: Date }
});
/*
 * Add slug
 */
recipeSchema.pre('validate', function (next) {
    this.slug = helpers.slug(this.name);
    next();
});


/*
 * Updated
 */
recipeSchema.pre('save', function (next) {
    this.updated = new Date().toISOString();
    next();
});


/**
 * Statics
 */

recipeSchema.statics = {

    /**
     * Find recpie by id
     *
     * @param {ObjectId} id
     * @param {Function} cb
     * @api private
     */

    load: function (id, cb) {
        this.findOne({ _id: id }).populate('user').populate('products').exec(cb);
    },

    /**
     *
     * @param slug - slug of recpie
     * @param cb
     */
    bySlug: function (slug, cb) {
        this.findOne({'slug': slug}).exec(cb);
    },

    /**
     * List of Recipes
     *
     * @param {Object} options
     * @param {Function} cb
     * @api private
     */

    list: function (options, cb) {
        var criteria = options.criteria || {}
        this.find(criteria)
            .populate('user').populate('products')
            .sort({'created': -1}) // sort by date
            .limit(options.perPage)
            .skip(options.perPage * options.page)
            .exec(cb)
    },

    /**
     * List of Recipes
     *
     * @param {Object} options
     * @param {Function} cb
     * @api private
     */

    search: function (options, cb) {
        var criteria = options.criteria || {}
//        var objects = ["52d58496e0dca1c710d9bfdd", "52d58da5e0dca1c710d9bfde", "52d58da5e0dca1c710d9bfde", "52d91cd69188818e3964917b"];
//        var s = {
//             products: { $in: objects }
//        };

        this.find(criteria)
            .populate('user').populate('products')
            .sort({'created': -1}) // sort by date
            .limit(options.perPage)
            .skip(options.perPage * options.page)
            .exec(cb)
    }

}


recipeSchema.post('init', function (doc) {
    console.log('%s has been initialized from the db', doc._id);
})
recipeSchema.post('validate', function (doc) {
    console.log('%s has been validated (but not saved yet)', doc._id);
})
recipeSchema.post('save', function (doc) {
    console.log('%s has been saved', doc._id);
})
recipeSchema.post('remove', function (doc) {
    console.log('%s has been removed', doc._id);
})


mongoose.model('Recipe', recipeSchema);