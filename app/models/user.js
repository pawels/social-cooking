/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
var Schema = mongoose.Schema;
var crypto = require('crypto');
var Product = mongoose.model('Product');
var Recipe = mongoose.model('Recipe');
// , _ = require('underscore')
var authTypes = ['twitter', 'facebook', 'google'];

/**
 * User Schema
 */

var UserSchema;
UserSchema = new Schema({
    name: String,
    email: String,
    username: String,
    provider: String,
    hashed_password: String,
    salt: String,
    facebook: {},
    twitter: {},
    google: {},
    created: { type: Date, default: Date.now },
    updated: { type: Date }
});

/**
 * Virtuals
 */

UserSchema
    .virtual('password')
    .set(function (password) {
        this._password = password
        this.salt = this.makeSalt()
        this.hashed_password = this.encryptPassword(password)
    })
    .get(function () {
        return this._password
    })

/**
 * Validations
 */

var validatePresenceOf = function (value) {
    console.info(value);
    return value && value.length
}

// the below 4 validations only apply if you are signing up traditionally

UserSchema.path('name').validate(function (name) {
    // if you are authenticating by any of the oauth strategies, don't validate
    if (authTypes.indexOf(this.provider) !== -1) return true
    return name.length
}, 'Name cannot be blank')

UserSchema.path('email').validate(function (email) {
    // if you are authenticating by any of the oauth strategies, don't validate
    if (authTypes.indexOf(this.provider) !== -1) return true
    return email.length
}, 'Email cannot be blank')

UserSchema.path('username').validate(function (username) {
    // if you are authenticating by any of the oauth strategies, don't validate
    if (authTypes.indexOf(this.provider) !== -1) return true
    return username.length
}, 'Username cannot be blank')

UserSchema.path('hashed_password').validate(function (hashed_password) {
    // if you are authenticating by any of the oauth strategies, don't validate
    if (authTypes.indexOf(this.provider) !== -1) return true
    return hashed_password.length
}, 'Password cannot be blank')
//var emailValidator = [validate({message: "Email Address should be between 5 and 64 characters"},'len', 5, 64), validate({message: "Email Address is not correct"},'isEmail')];

/**
 * Pre-save hook

 */
UserSchema.pre('save', function (next) {
    console.info(this);
    if (!this.isNew) return next()

    if (!validatePresenceOf(this.password)) {
        next(new Error('Invalid password'));
    }
    //&& authTypes.indexOf(this.provider) === -1)

    else
        next()
})

/**
 * Methods
 */

UserSchema.methods = {

    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} plainText
     * @return {Boolean}
     * @api public
     */

    authenticate: function (plainText) {
        return this.encryptPassword(plainText) === this.hashed_password
    },

    /**
     * Make salt
     *
     * @return {String}
     * @api public
     */

    makeSalt: function () {
        return Math.round((new Date().valueOf() * Math.random())) + ''
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @return {String}
     * @api public
     */

    encryptPassword: function (password) {
        if (!password) return ''
        return crypto.createHmac('sha1', this.salt).update(password).digest('hex')
    }
}

/**
 * Statics
 */

UserSchema.statics = {

    /**
     * Find product by email address
     *
     * @param {String} remail
     * @param {Function} cb
     * @api private
     */

    loadByEmail: function (email, cb) {
        this.findOne({ email: email })
            .exec(cb)
    },


    /**
     * Find product by id
     *
     * @param {ObjectId} id
     * @param {Function} cb
     * @api private
     */

    load: function (id, cb) {
        this.findOne({ _id: id })
            .exec(cb)
    },

    /**
     * List products
     *
     * @param {Object} options
     * @param {Function} cb
     * @api private
     */

    list: function (options, cb) {
        var criteria = options.criteria || {}
        this.find(criteria)
            .populate('product', 'name')
            .sort({'created': -1}) // sort by date
            .limit(options.perPage)
            .skip(options.perPage * options.page)
            .exec(cb)
    }

}

mongoose.model('User', UserSchema);
