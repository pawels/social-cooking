var express = require('express');
/**
 *
 * Autoryzacja basic http
 * @type {*}
 */
var auth = express.basicAuth(function(user, pass) {
    return (user == "socialcooking" && pass == "sdaf3@#$TGDFG4534q");
})



module.exports = function(app){
    //Dodanie autoryzacji basic do każdego requesta
    app.all('*', auth);
	var product = require('./app/controllers/product')(app);
	var user = require('./app/controllers/user')(app);
    var recipe = require('./app/controllers/recipes')(app);
    var application = require('./app/controllers/applications')(app);
}

